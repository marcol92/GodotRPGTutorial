extends Node2D

export(int) var wander_range = 32
export(int) var change_position_timeout = 3

onready var start_position = global_position
onready var target_position = start_position

onready var timer = $Timer

func _ready():
	update_target_position()
	timer.start(change_position_timeout)	

func update_target_position():
	var target_vector = Vector2(rand_range(-wander_range, wander_range), rand_range(-wander_range, wander_range))
	target_position = start_position + target_vector

func _on_Timer_timeout():
	update_target_position()
	timer.start(change_position_timeout)
