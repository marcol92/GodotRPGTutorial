extends Area2D

const HitEffect = preload("res://Resources/Effects/HitEffect.tscn")

export(bool) var show_hit = true

onready var timer = $Timer
onready var collisionShape = $CollisionShape2D

signal invincibility_started
signal invincibility_ended

func start_invincibility(duration):
	timer.start(duration)
	emit_signal("invincibility_started")

func create_hit_effect():
	var effect = HitEffect.instance()
	effect.global_position = global_position
	get_tree().current_scene.add_child(effect)

func _on_Timer_timeout():
	emit_signal("invincibility_ended")

func _on_Hurtbox_invincibility_started():
	collisionShape.set_deferred("disabled", true)

func _on_Hurtbox_invincibility_ended():
	collisionShape.disabled = false
